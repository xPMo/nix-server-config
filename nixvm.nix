{ config, pkgs, ... }:
{
  networking.hostName = "nixvm";
  imports = [ ./base.nix ];
}
