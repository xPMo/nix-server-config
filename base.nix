# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ data, config, pkgs, lib, ... }:

let
  tailnet = "mercat-hydra.ts.net";
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Nix
  nix.settings = {
    experimental-features = ["nix-command" "flakes"];
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  #networking.hostName = "${hostname}"; # Defined in {host}.nix
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # For running containers:
  networking.nat = {
    enable = true;
    internalInterfaces = ["ve-+"];
    externalInterface = "ens3";
    enableIPv6 = true;
  };
  networking = {
    bridges.br0.interfaces = [ "enp2s0" ]; # Adjust interface accordingly

    # Get bridge-ip with DHCP
    useDHCP = false;
    interfaces."br0".useDHCP = true;

    # Set bridge-ip static
    interfaces."br0".ipv4.addresses = [{
      address = "192.168.100.3";
      prefixLength = 24;
    }];
    defaultGateway = "192.168.100.1";
    nameservers = [ "192.168.100.1" ];
  };

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.pmo = {
    isNormalUser = true;
    description = "Gamma";
    shell = pkgs.zsh;
    extraGroups = [ "networkmanager" "wheel" ];
    openssh.authorizedKeys.keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINnY69j2Qe4+iELTqwsD9JEuuq5G2COIkaFIN16pI3B3 pmo@36xf"];
    packages = with pkgs; [ comma ];
  };

  # Enable automatic login for the user.
  services.getty.autologinUser = "pmo";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    fzf
    file
    git
    htop
    jq
    neovim
    nmap
    spice-vdagent
    tailscale
    tmux
    tree
    ripgrep
    wget
    zsh
  ];

  # Zsh
  programs.zsh.enable = true;
  environment.shells = with pkgs; [ zsh ];

  # Mosh
  programs.mosh.enable = true;

  # Editor
  environment.variables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable Tailscale
  services.tailscale = {
    enable = true;
    permitCertUid = "caddy";
    useRoutingFeatures = "both";
  };

  # Open ports in the firewall.
  networking.firewall = {
    # Only ssh public
    allowedTCPPorts = [ 22 443 8096 ];
    # allowedUDPPorts = [ ... ];

    # Everything else over Tailscale
    interfaces."tailscale0" = {
      allowedTCPPorts = [
        22     # SSH
        80 443 # HTTP
        2049   # NFS4
        8096   # Jellyfin
      ];
    };
  };

  # Jellyfin
  services.jellyfin.enable = true;
  # needs vaapi enabled on OS-level
  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
      vaapiIntel
      vaapiVdpau
      libvdpau-va-gl
      intel-compute-runtime # OpenCL filter support (hardware tonemapping and subtitle burn-in)
    ];
  };

  # Home Assistant
  services.home-assistant = {
    enable = true;
    extraComponents = [
      "esphome"
      "met"
      "radio_browser"
    ];
    config = {
      default_config = {
        # TODO
      };
    };
  };

  virtualisation.oci-containers = {
    backend = "podman";
    containers."wyze-bridge" = {
      environment = {
        TZ = "US/Central";
        WYZE_EMAIL = "";
        WYZE_PASSWORD = "";
        API_ID = "";
        API_KEY = "";
      };
      image = "mrlt8/wyze-bridge:latest";
      ports = [
        "8554:8554" # rtsp
        "8888:8888" # hls
        "8889:8889" # WebRTC
        "8189:8189/udp" # WebRTC/ICD
        "5000:5000" # web-ui
      ];
    };
  };

  # Nextcloud: Containerized
  containers.nextcloud = {
    autoStart = true;
    timeoutStartSec = "15min";
    #privateNetwork = true;
    hostBridge = "br0";
    localAddress = "192.168.100.5/24";
    hostAddress6 = "fc00::1";
    localAddress6 = "fc00::2";

    # Mountpoints: TODO: make not hard-coded
    bindMounts = {
      "/var/lib/nextcloud" = {
        hostPath = "/mnt/pool0/nextcloud";
        isReadOnly = false;
      };
      "/var/lib/postgresql" = {
        hostPath = "/mnt/pool0/postgres";
        isReadOnly = false;
      };
    };

    config = { config, pkgs, lib, ... }: {

      environment.systemPackages = with pkgs; [
        tailscale-nginx-auth
        lapack
      ];

      systemd.tmpfiles.rules = [ "d /var/lib/nextcloud 750 nextcloud nextcloud -" ];
      networking = {
        firewall = {
          enable = true;
          allowedTCPPorts = [ 80 443 ];
        };
        # Use systemd-resolved inside the container
        # Workaround for bug https://github.com/NixOS/nixpkgs/issues/162686
        useHostResolvConf = lib.mkForce false;
      };
      services.resolved.enable = true;

      # Enable Tailscale
      services.tailscale = {
        enable = true;
        permitCertUid = "caddy";
        useRoutingFeatures = "both";
        # Can't create /dev/tailscale0
        interfaceName = "userspace-networking";
      };

      # Enable Nextcloud
      services.nextcloud = {
        enable = true;
        package = pkgs.nextcloud29;
        hostName = "${config.networking.hostName}.${tailnet}";

        config.adminpassFile = "${pkgs.writeText "adminpass" "passwordfixme"}";
        configureRedis = true;
        maxUploadSize = "32G";

        config.dbtype = "pgsql";
        database.createLocally = true;

        poolSettings = {
          "pm" = "dynamic";
          "pm.max_children" = 100;
          "pm.start_servers" = 8;
          "pm.min_spare_servers" = 4;
          "pm.max_spare_servers" = 16;
        };

        settings = {
          loglevel = 1;
          dbtype = "pgsql";
          trusted_domains = [ "${config.networking.hostName}" ];
          trusted_proxies = ["127.0.0.1/8" "::1"];

          # debug = true;

          default_phone_region = "US";
          #overwriteprotocol = "https";
        };

        #extraApps = {
        #  inherit (config.services.nextcloud.package.packages.apps) contacts calendar tasks;
        #};

        phpOptions = {
          catch_workers_output = "yes";
          display_errors = "stderr";
          error_reporting = "E_ALL & ~E_DEPRECATED & ~E_STRICT";
          expose_php = "Off";

          "opcache.enable" = "1";
          "opcache.jit" = "1255";
          "opcache.jit_buffer_size" = "512M";
          "opcache.memory_consumption" = "512";
          "opcache.interned_strings_buffer" = "64";
          "opcache.max-accelerated_files" = "20000";
          "opcache.save_comments" = "1";

          # https://docs.nextcloud.com/server/stable/admin_manual/configuration_files/big_file_upload_configuration.html#configuring-php
          # > Output Buffering must be turned off [...] or PHP will return memory-related errors.
          output_buffering = "Off";

          # https://docs.nextcloud.com/server/21/admin_manual/configuration_server/caching_configuration.html#id2
          # Prevent corruption if Redis is used for session management
          "redis.session.locking_enabled" = "1";
          "redis.session.lock_retries" = "-1";
          "redis.session.lock_wait_time" = "10000";

          ## Enable for tracing
          "xdebug.trigger_value" = "Debugger";
          "xdebug.mode" = "profile,trace";
          "xdebug.output_dir" = "/var/log/xdebug";
          "xdebug.start_with_request" = "trigger";
        };

        #appstoreEnable = true;
        phpExtraExtensions = all: [ all.bz2 all.pdlib ];

      };

      # Use Caddy instead for tailscale integration
      services.nginx.enable = false;
      services.phpfpm.pools.nextcloud.settings = {
        "listen.owner" = config.services.caddy.user;
        "listen.group" = config.services.caddy.group;
      };

      # postgres recovery/checkpointing can take some time
      systemd.services.postgresql.serviceConfig.TimeoutStartSec = "14min";
      systemd.services.postgresql.serviceConfig.TimeoutStopSec = "8min";

      # Let the db come up first
      systemd.services.phpfpm-nextcloud.after = ["postgresql.service"];
      systemd.services.nextcloud-cron.after = ["postgresql.service"];

      # https://pgtune.leopard.in.ua
      services.postgresql.settings = {
        log_statement = "all";
        checkpoint_completion_target = 0.9;
        max_connections = 48;
        shared_buffers = "4GB";
        effective_cache_size = "12GB";
        work_mem = "10MB";
        max_worker_processes = 4;
        max_parallel_workers = 4;
        max_parallel_workers_per_gather = 2;
        max_parallel_maintenance_workers = 2;
        huge_pages = "try";
      };


      # Needed to read /var/lib/nextcloud
      users.groups.nextcloud.members = [ "nextcloud" config.services.caddy.user ];

      # Caddy
      systemd.services.caddy.after = ["tailscaled.service"];
      services.caddy = {
        enable = true;
        extraConfig = 
          ''
          ${config.networking.hostName}:80 {
            redir https://${config.networking.hostName}.${tailnet}
          }
          ${config.networking.hostName}.${tailnet} {

            root * ${config.services.nextcloud.package}
            root /store-apps/* /var/lib/nextcloud
            root /nix-apps/* /var/lib/nextcloud
            encode zstd gzip

            file_server

            header {
              Strict-Transport-Security max-age=31536000;
            }

            redir /.well-known/carddav /remote.php/dav 301
            redir /.well-known/caldav /remote.php/dav 301
            redir /.well-known/webfinger /index.php/.well-known/webfinger
            redir /.well-known/nodeinfo  /index.php/.well-known/nodeinfo
            @davClnt {
              header_regexp User-Agent ^DavClnt
              path /
            }

            redir @davClnt /remote.php/webdev{uri} 302
            @sensitive {
              # ^/(?:build|tests|config|lib|3rdparty|templates|data)(?:$|/)
              path /build     /build/*
              path /tests     /tests/*
              path /config    /config/*
              path /lib       /lib/*
              path /3rdparty  /3rdparty/*
              path /templates /templates/*
              path /data      /data/*

              # ^/(?:\.|autotest|occ|issue|indie|db_|console)
              path /.*
              path /autotest*
              path /occ*
              path /issue*
              path /indie*
              path /db_*
              path /console*
            }
            respond @sensitive 404

            php_fastcgi unix/${config.services.phpfpm.pools.nextcloud.socket} {
              env front_controller_active true
            }

            log
          }
          '';
      };

      system.stateVersion = "24.05"; # Did you read the comment?
      #system.autoUpgrade.enable = true;
      #system.autoUpgrade.allowReboot = true;
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = true;
}
