**TODO:**

- Clone `https://gitlab.com/xPMo/dotfiles.cli` into `/home/pmo`
- Secrets management (git-crypt?)
- Nextcloud
- NFS4
- ~~Serve both Nextcloud and Jellyfin over HTTPS~~
  - ~~Multiple tailscale instances to serve `nextcloud.my-tailnet.ts.net` and `jellyfin.my-tailnet.ts.net`?S~~
  - Just serve Nextcloud on root and Jellyfin on `/jellyfin`
