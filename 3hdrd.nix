{ config, pkgs, ... }:
let
  pool = "/mnt/pool0";
in
{
  networking.hostId = "00d0a65d";
  networking.hostName = "3hdrd";
  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.forceImportRoot = false;
  boot.zfs.extraPools = [ "pool0" ];
  services.nextcloud.home = "/mnt/pool0/nextcloud";
  services.postgresql.dataDir = "/mnt/pool0/postgres";
  imports = [ ./base.nix ];
  swapDevices = [ { device = "/.swapfile"; } ];

  # NFS
  fileSystems."/srv/nfs/media" = {
    device = "/mnt/pool0/media";
    options = [ "bind" ];
  };
  # TODO: Include only tailscale hosts for DiD
  services.nfs.server.enable = true;
  services.nfs.server.exports = ''
     /srv/nfs         *(rw,fsid=0,no_subtree_check)
     /srv/nfs/media   *(rw,sync)
  '';
}
